* UX improvements when tests start running:
* Indeterminate progress bar added
* ETA calculation displayed
* Bug fixes to enable the display of preferences
* Fixed a UI glitch in the Websites Categories settings