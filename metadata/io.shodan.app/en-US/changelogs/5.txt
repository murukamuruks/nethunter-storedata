* We upgraded our ionic version
* We got a lighter APK (from 7.47MB to 5.10MB)
* We removed the Camera permission so you will have to copy/paste your Shodan API key from now on.
* We added the following capabilities :
- able to bookmark a search and a host
- able to see in the search results if some hosts are affected by some CVEs
- we are also storing the previous searches so that you can easily find them back