* Upgrade target to Android 9
* Upgrade several inner dependencies
* Fixed several crashes
